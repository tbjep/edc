use std::{env, path::{Path, PathBuf}, error::Error, fs::File, io::{BufReader, Read, self}};

#[derive(Debug)]
enum BufferName {
    Path(PathBuf),
    String(String),
}

#[derive(Debug)]
struct EditBuffer {
    contents: String,
    name: BufferName,
}

impl EditBuffer {
    fn open_file(path: &Path) -> Result<EditBuffer, io::Error> {
        let file = File::open(path)?;
        let mut buffer = EditBuffer{
            name: BufferName::Path(path.to_path_buf()),
            contents: String::new(),
        };
        let mut buf_reader = BufReader::new(file);
        buf_reader.read_to_string(&mut buffer.contents)?;
        Ok(buffer)
    }
}

struct BufferList {
    buffers: Vec<EditBuffer>,
    current_buffer: usize,
}

impl BufferList {
    fn new(buffers: Vec<EditBuffer>) -> BufferList {
        BufferList{
            buffers,
            current_buffer: 0
        }
    }

    fn current(&self) -> Option<&EditBuffer> {
        self.buffers.get(self.current_buffer)
    }
    fn next(&mut self) {
        self.current_buffer += 1
    }
    fn prev(&mut self) {
        self.current_buffer -= 1
    }
}

struct EditorState {
    buffers: BufferList
}

fn editior_loop(arg_buffers: Vec<EditBuffer>) -> Result<(), Box<dyn Error>> {
    let mut state = EditorState{
        buffers: BufferList::new(arg_buffers),
    };
    let mut input = String::new();
    loop {
        io::stdin().read_line(&mut input)?;
        match input.trim() {
            "n" => state.buffers.next(),
            "N" => state.buffers.prev(),
            "p" => println!("{}", match state.buffers.current() { Some(b) => b.contents.as_str(), None => "buffer not available"}),
            "q" => break,
            _ => (),
        }
    }
    Ok(())
}

fn main() -> Result<(), Box<dyn Error>> {
    let args: Vec<String> = env::args().collect();
    let arg_buffers = args
        .split_first().ok_or("")?.1
        .iter()
        .map(Path::new)
        .map(EditBuffer::open_file)
        .filter_map(Result::ok)
        .collect();
    println!("{:?}", arg_buffers);
    editior_loop(arg_buffers)
}
